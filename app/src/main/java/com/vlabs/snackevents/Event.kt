package com.vlabs.snackevents

data class Event(val message: String)
package com.vlabs.snackevents

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.TextView

class MainActivity : AppCompatActivity() {

    private val repo: EventsRepo by inject()

    private val root by lazy { findViewById<ConstraintLayout>(R.id.root_layout) }

    private val eventsCount by lazy { findViewById<TextView>(R.id.events_count) }
    private val enqueue by lazy { findViewById<View>(R.id.enqueue_event) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        repo.topEvent().observe(this, Observer {
            it?.let {
                Snackbar
                        .make(root, it.message, Snackbar.LENGTH_LONG)
                        .addCallback(consumeEventOnDismiss())
                        .show()
            }
        })

        repo.eventsCount().observe(this, Observer {
            eventsCount.text = it.toString()
        })

        var eventNumber = 0
        enqueue.setOnClickListener {
            repo.enqueue(Event("Event $eventNumber"))
            ++eventNumber
        }
    }

    private fun consumeEventOnDismiss(): Snackbar.Callback {
        return object : Snackbar.Callback() {
            override fun onDismissed(transientBottomBar: Snackbar?, event: Int) {
                repo.dismissTop()
            }
        }
    }
}

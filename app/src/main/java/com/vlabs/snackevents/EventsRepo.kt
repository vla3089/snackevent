package com.vlabs.snackevents

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData

class EventsRepo {

    private val queue: MutableList<Event> = mutableListOf()

    private val eventsCount = MutableLiveData<Int>()

    private val topEvent = MutableLiveData<Event?>()

    fun enqueue(event: Event) {
        queue.add(event)
        notifyEventsCountChanged() // todo: get rid of a side effect

        if (queue.size == 1) {
            topEvent.postValue(event)
        }
    }

    fun topEvent() : LiveData<Event?> = topEvent

    fun dismissTop() {
        if (queue.isNotEmpty()) {
            queue.removeAt(0)
            topEvent.postValue(peek())
            notifyEventsCountChanged() // todo: get rid of a side effect
        }
    }

    fun eventsCount(): LiveData<Int> = eventsCount

    private fun notifyEventsCountChanged() {
        eventsCount.postValue(queue.size)
    }

    private fun peek(): Event? = queue.firstOrNull()

}
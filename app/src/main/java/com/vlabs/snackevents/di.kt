package com.vlabs.snackevents

fun inject() = lazy(LazyThreadSafetyMode.NONE) { singleRepo }

private val singleRepo = EventsRepo()